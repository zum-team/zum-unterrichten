Background
==========

Fundamental Problem
-------------------
 * mediawiki is fairly structure less hard to integrate structured metadata
 * multiple wikis in ZUM e.V.
 * apps.zum.de is another OER content source for ZUM e.V.
 * metadata vocabulary is not yet clear cut
   * jointly is working on fixed keywords -> SKOS https://jointly.info/announcement/kontrollierte-vokabulare-wieso-weshalb-warum/#



edutags.de (ET)
-------------------

 * current situation (probably)
   * ET scrapes the wiki recent changes or page listing for category:ZUM2Edutags
   * ET then scrapes the page looking for `<meta keyword="">` tags
   * wiki uses [YetAnotherKeyword](https://www.mediawiki.org/wiki/Extension:YetAnotherKeywords) Extension
 
wirlernenonline.de (WOL)
-------------------

 * uses some kind of [LOM vocabulary](https://en.wikipedia.org/wiki/Learning_object_metadata)
 * uses [ZUM Spider](https://github.com/openeduhub/oeh-search-backend/blob/master/etl/converter/spiders/zum_spider.py) 
   to extract information from the wiki pages
   * does something weird, but probably necessary with the page.item.links to get categories (instead of using page.item.categories)
   * categories are matched against wirlernenonline.de tags (probably to prevent misspellings)
     * https://wirlernenonline.de/wp-json/wp/v2/tags/?per_page=100&_fields=name
   * categories are further matched against some vocabularies to extract specific metadata
     * Schulfach: https://vocabs.openeduhub.de/w3id.org/openeduhub/vocabs/discipline/index.html
     * Bildungsstufe: https://vocabs.openeduhub.de/w3id.org/openeduhub/vocabs/educationalContext/index.html
     * Zielgruppe: https://vocabs.openeduhub.de/w3id.org/openeduhub/vocabs/intendedEndUserRole/index.html 
 * can extract [LRMI](https://www.dublincore.org/specifications/lrmi/lrmi_1/) in simple json-ld script blocks
   * does not do full LRMI normalization but naive implementation
 * supports oai
   * https://github.com/openeduhub/oeh-search-etl/blob/master/converter/spiders/oai_base.py


 
Idea Proposal
=============
Base Document for approaching Freelancers:
https://docs.google.com/document/d/1SlJ6RiklWXMNY_mPH8UdyaTxcCfSsloeg9-V6T8C_Po/edit?usp=sharing

 * solid low-maintenance OAI-PHM provider for ZUM
   * OAI-PHM is a de-facto standard to make content available to repositories with strong metadata outfit
   * is not fixated on a single metadata vocabulary but allows multiple ones at the same time
   * used by many repositories to harvest content from other sources and make it available to their users
 * ❕ most OAI-PHM repositories are part of a larger content repo software and need substantial effort for integration
 * ❇ use https://github.com/opencultureconsulting/oai_pmh
   * file based repository; takes a folder of oai records and provides them as a repository
   * allows arbitrary metadata
   * simple operating model -> generate oai-record file and put it in the correct place
   * succesive integration possible by having bots/spiders/crawlers that generate the oai-records or rewrite existing records for other metadata vocabularies (e.g. dc -> lrmi)
   * xml allows relatively easy extensability due to namespaces
   * prototype:
     * repository
     * crawler that checks specially marked pages in zum-unterrichten (category search via api) and generates list of candidates to create records for
     * generator takes a candidate and generates the oai record 
       * title
       * description
       * authors
       * license
       * categories
  * 🔮 crawler for apps.zum.de using the drupal api
  * 🔮 ideas for vocabulary alignment
    * bot that auto-generates glue-metadata in the oai records
      * e.g. when `Sekundardstufe II` is found it generates `Sekundarstufe 2` category
      * 💢 (the glue information would not be available in the content source even though it would be helpful there)
    * mediawiki category page contains some info for alternative categories to generate
      * 💢 (mediawiki specific, would not work with apps.zum.de)
    * mediawiki uses templates that add the zum category and the glue categories (which are made hidden) see #2
      * 💢 (mediawiki specific, would not work with apps.zum.de)
  * 🔮 huge synergy with real metadata in mediawiki from #8 and maybe integration of [Skohub Editor](https://github.com/hbz/skohub-editor) into mediawiki VE


 


